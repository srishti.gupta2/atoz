from pymongo import MongoClient
import time
import datetime as dt
import pandas as pd
from dotenv import dotenv_values

# Requires the PyMongo package.
# https://api.mongodb.com/python/current

client = MongoClient('mongodb://dl-user-1:A2mClcihXGlNQC7M@datalake0-3comg.a.query.mongodb.net/Database0?ssl=true&authSource=admin')
result = client['Database0']['Order_Commission_Invoice_4'].aggregate([
    {
        '$match': {
            'payment_status.code': 'success'
        }
    }, {
        '$lookup': {
            'from': 'catalog', 
            'localField': 'items.catalog_id', 
            'foreignField': '_id', 
            'as': 'catalog'
        }
    }, {
        '$unwind': {
            'path': '$catalog'
        }
    }, {
        '$set': {
            'catalog.category_path': {
                '$map': {
                    'input': '$catalog.category_path', 
                    'as': 'category_path', 
                    'in': {
                        '$split': [
                            {
                                '$trim': {
                                    'input': '$$category_path', 
                                    'chars': '/'
                                }
                            }, '/'
                        ]
                    }
                }
            }
        }
    }, {
        '$project': {
            'Order Date': '$created_at', 
            'Order Time': '$created_at', 
            'Order ID': '$order_id', 
            'Receipt ID': '$invoice_info.invoice_no', 
            'Shipping Display Name': '$invoice_info.shipping_address.display_name', 
            'User Account Name': '$user_info.full_name', 
            'Customer Phone': '$invoice_info.shipping_address.phone_number.number', 
            'Shipping Address': '$invoice_info.shipping_address.line1', 
            'Shipping Address Pincode': '$invoice_info.shipping_address.postal_code', 
            'Shipping Address City': '$invoice_info.shipping_address.city', 
            'Shipping Address State': '$invoice_info.shipping_address.state.name', 
            'User Email': '$invoice_info.user_info.email', 
            'User Phone': '$invoice_info.user_info.phone_no.number', 
            'Product Name': '$items.catalog_info.name', 
            'Brand Name': '$brand_info.name', 
            'Registered Brand Name': '$brand_info.registered_name', 
            'Product SKU': '$items.catalog_info.variant.sku', 
            'Variant Attribute': '$items.catalog_info.variant.attribute', 
            'Base Price': '$items.base_price.value', 
            'Retail Price': '$items.retail_price.value', 
            'Quantity': '$items.quantity', 
            'Discount Coupon Code': '$coupon_info.code', 
            'Offer Value': '$items.offer_value.value', 
            'Discount Coupon Total Value': '$coupon_info.value.value', 
            'Discount Coupon Applied Value': '$coupon_info.applied_value.value', 
            'Final Selling Price': {
                '$subtract': [
                    '$items.total_price.value', {
                        '$max': [
                            '$items.offer_value.value', 0
                        ]
                    }
                ]
            }, 
            'Tax Type': '$items.tax.type', 
            'Tax Rate': '$items.tax.rate', 
            'Payment Source': '$payment_mode', 
            'Payment Mode': {
                '$cond': {
                    'if': {
                        '$eq': [
                            '$payment_mode', 'cod'
                        ]
                    }, 
                    'then': 'cod', 
                    'else': 'prepaid'
                }
            }, 
            'Platform': '$platform', 
            'Source Type': '$source', 
            'Creator Name': '$items.influencer_name', 
            'Creator Commission Value': '$items.commission_info.commission_value', 
            'Username of Creator': '$items.influencer_username', 
            'Store link of Creator': '$items.store_link', 
            'Category Paths': '$catalog.category_path', 
            'Uploaded Brand Commission': '$catalog.brand_commission_rate', 
            'Uploaded Creator Commission': '$catalog.commission_rate', 
            'Current Order Status': '$order_status.code', 
            'Courier Name': '$courier_name', 
            'Tracking ID': '$tracking_id'
        }
    }
])

df = pd.DataFrame(result)
df['Order ID']=df['Order ID'].astype(int)
resp = client['Database0']['prod_catalog.category'].find({})
category_resp = list(resp)

top_lvl1, top_lvl2, top_lvl3 = list(), list(), list()
for item in df['Category Paths']:
  lvl1, lvl2, lvl3 = list(), list(), list()
  for category in item:
    lvl1.append(category[0])
    lvl2.append(category[1])
    lvl3.append(category[2])
  top_lvl1.append(lvl1)
  top_lvl2.append(lvl2)
  top_lvl3.append(lvl3)

df["Category L1"] = top_lvl1
df["Category L2"] = top_lvl2
df["Category L3"] = top_lvl3

category_dict = dict()
for value in list(category_resp):
    category_dict[str(value["_id"])] = value['name']

def get_category_name(x):
    res = list()
    for i in x:
        res.append(category_dict[i])
    return res

df['Category L1'] = df['Category L1'].apply(get_category_name)
df['Category L2'] = df['Category L2'].apply(get_category_name)
df['Category L3'] = df['Category L3'].apply(get_category_name)

df = df.drop('Category Paths', axis = 1)

import numpy as np
df['Category L1']=df['Category L1'].apply(lambda x: list(np.unique(x)))
df['Category L2']=df['Category L2'].apply(lambda x: list(np.unique(x)))
df['Category L3']=df['Category L3'].apply(lambda x: list(np.unique(x)))

df['Order Date'] = df['Order Date'].apply(lambda x: (x + dt.timedelta(hours=5, minutes=30)))
df['Order Time'] = df['Order Time'].apply(lambda x: (x + dt.timedelta(hours=5, minutes=30)).strftime("%H:%M"))
df = df[['Order ID','Receipt ID', 'Order Date', 'Order Time', 'Customer Name', 'Customer Phone', 'Shipping Address', 'Shipping Address Pincode', 'Shipping Address City', 'Shipping Address State', 'User Email', 'User Phone', 'Product Name', 'Brand Name', 'Registered Brand Name', 'Product SKU', 'Variant Attribute', 'Base Price', 'Retail Price','Offer Value', 'Quantity', 'Discount Coupon Code', 'Discount Coupon Total Value', 'Discount Coupon Applied Value', 'Final Selling Price', 'Tax Type', 'Tax Rate', 'Payment Source', 'Payment Mode', 'Platform', 'Source Type', 'Creator Name', 'Creator Commission Value', 'Username of Creator', 'Store link of Creator', 'Category L1', 'Category L2', 'Category L3', 'Uploaded Brand Commission', 'Uploaded Creator Commission', 'Current Order Status', 'Courier Name', 'Tracking ID', 'Courier Partner' ]]

df.to_csv("/home/ubuntu/charts_by_srishti/atoz.csv")
time.sleep(600)